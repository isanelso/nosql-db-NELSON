function(doc) {
    if(doc.conflicts=true) {
        emit(doc._id, doc.name);
    }
}