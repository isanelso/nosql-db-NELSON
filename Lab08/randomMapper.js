function(doc) {
    if(doc.random && doc.name) {
        emit(doc.random, doc.name);
    }
}
