What is  cursor?: -> A pointer to the result set of a query. Clients can iterate through a cursor to retrieve results. By default, cursors timeout after 10 minutes of inactivity

https://docs.mongodb.com/manual/reference/glossary/#term-cursor

