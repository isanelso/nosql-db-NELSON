CREATE RULE venue_rules AS ON DELETE TO venues DO INSTEAD
UPDATE venues
SET active = ‘f’;
