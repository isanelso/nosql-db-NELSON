CREATE TABLE recipes (
name text NOT NULL,
description text NOT NULL,
ingredients text[],
ingredientquant int[],
instructions text NOT NULL,
PRIMARY KEY (name)
);