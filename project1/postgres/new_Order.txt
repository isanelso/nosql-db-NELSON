



CREATE OR REPLACE FUNCTION new_order(firstName text, lastName text, orderrecipe text)
RETURNS text AS $$
DECLARE
	arsize int;
	count int;
	ingname text[];
	ingQuant int[];
	enough int;
	currentName text;
	currentQuant int;
BEGIN
arsize = (SELECT array_length(ingredients, 1) FROM recipes WHERE name = orderrecipe);
ingname = (SELECT ingredients FROM recipes WHERE name = orderrecipe);
ingQuant = (SELECT ingredientquant FROM recipes WHERE name = orderrecipe);
count = 1;
enough =0;
IF (SELECT EXISTS(SELECT first_name FROM users WHERE first_name = firstName)) THEN
INSERT INTO orders (first_name, last_name, recipe, time) VALUES (firstName, lastName, orderrecipe, localtimestamp);
ELSE
RETURN 'user does not exist';
END IF;
<<check>> 
LOOP
currentName= (SELECT ingredients[count] FROM recipes WHERE name = orderrecipe);
IF(SELECT EXISTS(SELECT quantity FROM ingredients WHERE name = currentName)) THEN
currentQuant= (SELECT quantity FROM ingredients WHERE name = currentName);
ELSE
RETURN 'ingredient does not exist';
END IF;
IF(currentQuant< ingQuant[count]) THEN 
enough =1;
END IF;
count = count+1;
	EXIT WHEN count > arsize;
END LOOP;
count=0;
<<update>> 
LOOP
currentName= (SELECT ingredients[count] FROM recipes WHERE name = orderrecipe);
currentQuant= (SELECT quantity FROM ingredients WHERE name = currentName);
IF(enough=1) THEN 
RETURN 'Not enough ingredients to place order';
ELSE
UPDATE ingredients SET quantity = currentQuant-ingQuant[count] WHERE name = currentName;
END IF;
count = count+1;
	EXIT WHEN count > arsize;
END LOOP;
RETURN 'order placed';
END; $$
LANGUAGE  PLPGSQL;

